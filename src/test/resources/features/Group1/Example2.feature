Feature: Able to search a value
  So that search results are retrieved

  As a internet user

  @Issue("bugId-200")
  @TestCaseId("example-20")
  @SeverityLevel.BLOCKER
  @smoke
  Scenario: Successfully search in internet

    Given I navigate to bing

    When I search for "rupesh"

    Then I should be able to view search results