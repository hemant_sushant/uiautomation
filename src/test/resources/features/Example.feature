@family
Feature: Able to search for any text

  So that search results are retrieved

  As a internet user

  @Issue("bugId-108")
  @TestCaseId("example-99")
  @SeverityLevel.CRITICAL
  @smoke
  Scenario: Successfully search in internet

    Given I navigate to bing

    When I search for "hemant"

    Then I should be able to view search results
  @sample
  Scenario Outline: Successfully search in internet

    Given I navigate to bing

    When I search for "paru"

    Then I should be able to view search results
    And I search for "<text1>" and "<text2>"
  Examples:
  |text1|text2|
  |hemant123  |new1|
  |rupesh123  |new2|