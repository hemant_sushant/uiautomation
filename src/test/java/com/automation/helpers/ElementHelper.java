package com.automation.helpers;

import com.automation.constants.RuntimeConstants;
import com.automation.model.ElementIdentifierType;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.apache.log4j.Logger;
import java.util.List;

/**
 * Created by Hemant Sushant on 14/08/2016.
 */
public class ElementHelper {
    private WebDriver driver;
    private static int globalTimeout;
    final static Logger logger = Logger.getLogger(ElementHelper.class);

    public ElementHelper(WebDriver driver) {
        this.driver = driver;
        try {
            globalTimeout =Integer.parseInt(System.getProperty(RuntimeConstants.TIMEOUT_KEY));
            logger.info("Global timeout value set to "+globalTimeout);
        } catch (NumberFormatException e) {
            logger.error("Timeout must be a number. Invalid globalTimeout set as "+System.getProperty(RuntimeConstants.TIMEOUT_KEY),e);
            throw new RuntimeException("Timeout must be a number. Invalid globalTimeout set as "+System.getProperty(RuntimeConstants.TIMEOUT_KEY),e);
        }
    }

    public WebElement getElement(ElementIdentifierType idType, String id, int timeout){
        if (timeout<=0){
            timeout=globalTimeout;
        }
        WebDriverWait driverWait = new WebDriverWait(driver, timeout);
        return driverWait.until(ExpectedConditions.visibilityOfElementLocated(getLocator(idType,id)));
    }

    public WebElement getClickableElement(ElementIdentifierType idType, String id, int timeout){
        if (timeout<=0){
            timeout=globalTimeout;
        }
        WebDriverWait driverWait = new WebDriverWait(driver, timeout);
        return driverWait.until(ExpectedConditions.elementToBeClickable(getLocator(idType,id)));
    }

    public List<WebElement> getElementsWithCount(ElementIdentifierType idType, String id, int count, int timeout){
        if (timeout<=0){
            timeout=globalTimeout;
        }
        WebDriverWait driverWait = new WebDriverWait(driver, timeout);
        return driverWait.until(ExpectedConditions.numberOfElementsToBe(getLocator(idType,id),count));
    }

    public Boolean waitForElementInvisibility(ElementIdentifierType idType, String id, int timeout){
        if (timeout<=0){
            timeout=globalTimeout;
        }
        WebDriverWait driverWait = new WebDriverWait(driver, timeout);
        return driverWait.until(ExpectedConditions.invisibilityOfElementLocated(getLocator(idType,id)));
    }

    public List<WebElement> getElements(ElementIdentifierType idType, String id, int timeout){
        if (timeout<=0){
            timeout=globalTimeout;
        }
        WebDriverWait driverWait = new WebDriverWait(driver, timeout);
        return driverWait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(getLocator(idType,id)));
    }

    public WebDriver getDriver(){
        return driver;
    }

    private By getLocator(ElementIdentifierType idType, String id){
        if (idType==null){
            throw new RuntimeException("Invalid value set for element Identifier having id "+id);
        }
        By locator;
        switch (idType){
            case ID:
                locator = By.id(id);
                break;
            case NAME:
                locator = By.name(id);
                break;
            case CLASSNAME:
                locator = By.className(id);
                break;
            case LINKTEXT:
                locator = By.linkText(id);
                break;
            case PARTIALLINKTEXT:
                locator = By.partialLinkText(id);
                break;
            case XPATH:
                locator = By.xpath(id);
                break;
            case CSSSELECTOR:
                locator = By.cssSelector(id);
                break;
            case TAGNAME:
                locator = By.tagName(id);
                break;
            default:
                throw new IllegalArgumentException("Element selection criteria specified is "+idType+" and is invalid");
        }
        return locator;
    }
}
