package com.automation.helpers;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;
import java.util.Properties;

/**
 * Created by Hemant Sushant on 24/07/2016.
 */
public class PropertiesHelper {
    Properties prop = new Properties();

    public PropertiesHelper() {
    }

    public PropertiesHelper(String fileName) {
        InputStream input = null;
        try{
            input = PropertiesHelper.class.getClassLoader().getResourceAsStream(fileName);
            if(input==null){
                throw new RuntimeException("Unable to find configuration required - " + fileName);
            }
            //load a properties file from class path, inside static method
            prop.load(input);
        }catch (IOException ex){
            ex.printStackTrace();
        } finally{
            if(input!=null){
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public String getProperty(String propertyName){
        return prop.getProperty(propertyName);
    }

    public void generatePropertyFile(String filePath, Map<String,String> propertyMap){
        OutputStream output=null;
        try {
            output = new FileOutputStream(System.getProperty("user.dir")+System.getProperty("file.separator")+filePath);
            //set property values
            for (Map.Entry<String,String> entry : propertyMap.entrySet()) {
                String value="";
                if(entry.getValue()!=null){
                    value=entry.getValue();
                }
                prop.setProperty(entry.getKey(),value);
            }
            // save properties to folder defined
            prop.store(output, "Auto-Generated property file");
        } catch (IOException io) {
            System.out.println("######Error generating "+filePath+"######");
            io.printStackTrace();
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
