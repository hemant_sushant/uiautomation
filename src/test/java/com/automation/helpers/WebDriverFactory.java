package com.automation.helpers;

import com.automation.constants.RuntimeConstants;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;

import java.util.logging.Level;

/**
 * Created by Hemant Sushant on 14/08/2016.
 */
public class WebDriverFactory {
    private static int browserHeight=0;
    private static int browserWidth=0;

    public static WebDriver createWebDriver(String browserType){
        WebDriver driver;
        DesiredCapabilities capabilities;
        LoggingPreferences logPrefs = new LoggingPreferences();
        logPrefs.enable(LogType.DRIVER, Level.INFO);
        logPrefs.enable(LogType.BROWSER, Level.INFO);
        InitialiseBrowserDimensions();
        switch (browserType.toUpperCase()){
            case "FIREFOX":
                capabilities = DesiredCapabilities.firefox();
                capabilities.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
                capabilities.setCapability("marionette", true);
                driver=new FirefoxDriver(capabilities);
                break;
            case "CHROME":
                capabilities = DesiredCapabilities.chrome();
                capabilities.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
                driver=new ChromeDriver(capabilities);
                break;
            case "OPERA":
                capabilities = DesiredCapabilities.operaBlink();
                capabilities.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
                if (browserHeight!=0 && browserWidth !=0){
                    capabilities.setCapability("opera.arguments", "-screenwidth "+browserWidth+" -screenheight "+browserHeight);
                }
                driver=new OperaDriver(capabilities);
                break;
            case "EDGE":
                capabilities = DesiredCapabilities.edge();
                capabilities.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
                driver=new EdgeDriver(capabilities);
                break;
            case "IE":
                capabilities = DesiredCapabilities.internetExplorer();
                capabilities.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
                driver=new InternetExplorerDriver(capabilities);
                break;
            case "SAFARI":
                capabilities = DesiredCapabilities.safari();
                capabilities.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
                driver=new SafariDriver(capabilities);
                break;
            case "GHOST":
                capabilities = DesiredCapabilities.phantomjs();
                capabilities.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
                driver=new PhantomJSDriver(capabilities);
                break;
            //TODO
            /*case "REMOTE":
                driver=new RemoteWebDriver();
                break;*/
            default:
                throw new IllegalArgumentException("Browser type specified: "+browserType+" is not available");
        }
        //setting browser resolution based on commandline parameters browser.height and browser.width
        //TODO: Initialise all property file variables as system properties at the start of test execution
        if (browserWidth==0 || browserHeight==0) {
            driver.manage().window().maximize();
        }else{
            //setting browser size for all browsers except opera, opera changes done at time of browser creation
            if (!System.getProperty(RuntimeConstants.BROWSER_KEY).equalsIgnoreCase("opera")) {
                driver.manage().window().setSize(new Dimension(browserWidth, browserHeight));
            }
        }
        return driver;
    }

    private static void InitialiseBrowserDimensions() {
        if (System.getProperty(RuntimeConstants.BROWSER_HEIGHT_KEY) != null && !System.getProperty(RuntimeConstants.BROWSER_HEIGHT_KEY).isEmpty()
                && System.getProperty(RuntimeConstants.BROWSER_WIDTH_KEY) != null && !System.getProperty(RuntimeConstants.BROWSER_WIDTH_KEY).isEmpty()) {
            try {
                browserWidth = Integer.parseInt(System.getProperty(RuntimeConstants.BROWSER_WIDTH_KEY));
                browserHeight = Integer.parseInt(System.getProperty(RuntimeConstants.BROWSER_HEIGHT_KEY));
            } catch (NumberFormatException nfe) {
                System.out.println("Browser height and width specified are invalid, reverting to windows maximise behavior.");
                nfe.getMessage();
            }

        }
    }
}
