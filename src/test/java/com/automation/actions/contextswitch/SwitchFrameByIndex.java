package com.automation.actions.contextswitch;

import com.automation.actions.Action;
import com.automation.helpers.ElementHelper;
import com.automation.model.ElementIdentifierType;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by Hemant Sushant on 12/10/2016.
 */
public class SwitchFrameByIndex implements Action {
    @Override
    @Step("Switching frame by index {3}")
    public void execute(ElementHelper elementHelper, ElementIdentifierType idType, String id, String testData, int timeout) {
        int frameId=0;
        try {
            frameId=Integer.parseInt(testData);
        } catch (NumberFormatException nfe) {
            throw new RuntimeException("Please enter a valid number for frameId. Value entered is "+testData.trim(),nfe);
        }
        elementHelper.getDriver().switchTo().frame(frameId);
    }
}
