package com.automation.actions.contextswitch;

import com.automation.actions.Action;
import com.automation.helpers.ElementHelper;
import com.automation.model.ElementIdentifierType;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by Hemant Sushant on 12/10/2016.
 */
public class SwitchFrameByName implements Action {
    @Override
    @Step("Switching frame by name/id {3}")
    public void execute(ElementHelper elementHelper, ElementIdentifierType idType, String id, String testData, int timeout) {
        elementHelper.getDriver().switchTo().frame(testData);
    }
}
