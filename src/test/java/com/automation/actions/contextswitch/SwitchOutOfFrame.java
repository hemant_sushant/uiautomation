package com.automation.actions.contextswitch;

import com.automation.actions.Action;
import com.automation.helpers.ElementHelper;
import com.automation.model.ElementIdentifierType;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by Hemant Sushant on 12/10/2016.
 */
public class SwitchOutOfFrame implements Action {
    @Override
    @Step("Switching out of frames in page")
    public void execute(ElementHelper elementHelper, ElementIdentifierType idType, String id, String testData, int timeout) {
        elementHelper.getDriver().switchTo().defaultContent();
    }
}
