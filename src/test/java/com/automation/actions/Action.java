package com.automation.actions;

import com.automation.helpers.ElementHelper;
import com.automation.model.ElementIdentifierType;
import ru.yandex.qatools.allure.annotations.Step;

import java.lang.String;
/**
 * Created by Hemant Sushant on 23/08/2016.
 */
public interface Action {
    @Step
    public abstract void execute(ElementHelper elementHelper, ElementIdentifierType idType, String id, String testData, int timeout);
}
