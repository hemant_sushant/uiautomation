package com.automation.actions.utils;

import com.automation.actions.Action;
import com.automation.helpers.ElementHelper;
import com.automation.model.ElementIdentifierType;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by Hemant Sushant on 12/10/2016.
 */
public class StaticWait implements Action {
    @Override
    @Step("Wait for {3} seconds")
    public void execute(ElementHelper elementHelper, ElementIdentifierType idType, String id, String testData, int timeout) {
        int waitTime=0;
        try {
            waitTime=Integer.parseInt(testData.trim());
            Thread.sleep(waitTime*1000);
        } catch (NumberFormatException nfe) {
            throw new RuntimeException("Please enter a valid number for static wait. Value entered is "+testData.trim(),nfe);
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }
    }
}
