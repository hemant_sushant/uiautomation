package com.automation.actions.assertions;

import com.automation.actions.Action;
import com.automation.helpers.ElementHelper;
import com.automation.model.ElementIdentifierType;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by Hemant Sushant on 15/10/2016.
 */
public class ValidateElementCount implements Action {
    @Override
    @Step("Validate element count with id {2} as {3}")
    public void execute(ElementHelper elementHelper, ElementIdentifierType idType, String id, String testData, int timeout) {
        try {
            elementHelper.getElementsWithCount(idType,id,Integer.parseInt(testData), timeout);
        } catch (NumberFormatException nfe) {
            throw new RuntimeException("Please provide a valid number to validate element count",nfe);
        }
    }
}
