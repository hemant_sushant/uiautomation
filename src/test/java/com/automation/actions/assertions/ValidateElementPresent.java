package com.automation.actions.assertions;

import com.automation.actions.Action;
import com.automation.helpers.ElementHelper;
import com.automation.model.ElementIdentifierType;
import org.testng.Assert;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by Hemant Sushant on 23/08/2016.
 */
public class ValidateElementPresent implements Action {
    @Override
    @Step("Validate {2} is present and visible")
    public void execute(ElementHelper elementHelper, ElementIdentifierType idType, String id, String testData, int timeout) {
        Assert.assertTrue(elementHelper.getElement(idType,id, timeout).isDisplayed());
    }
}
