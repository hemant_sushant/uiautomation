package com.automation.actions.assertions;

import com.automation.actions.Action;
import com.automation.helpers.ElementHelper;
import com.automation.model.ElementIdentifierType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.List;

/**
 * Created by Hemant Sushant on 15/10/2016.
 */
public class ValidateTextNotInDropdown implements Action {
    @Override
    @Step("Validate options {3} not present in {2}")
    public void execute(ElementHelper elementHelper, ElementIdentifierType idType, String id, String testData, int timeout) {
        Select select = new Select(elementHelper.getElement(idType, id, timeout));
        List<WebElement> options = select.getOptions();
        String[] values = testData.split(",");
        boolean found=false;
        for (String value:values) {
            for (WebElement option:options){
                if(option.getText().equalsIgnoreCase(value.trim())){
                    found = true;
                    break;
                }
            }
            if(found){
                Assert.fail(value+" present in dropdown identified by "+id+"("+idType+")");
            }
            found=false;
        }
    }
}
