package com.automation.actions.assertions;

import com.automation.actions.Action;
import com.automation.helpers.ElementHelper;
import com.automation.model.ElementIdentifierType;
import org.junit.Assert;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by Hemant Sushant on 1/09/2016.
 */
public class ValidateElementInvisibility implements Action{

    @Override
    @Step("Validate {2} is invisible")
    public void execute(ElementHelper elementHelper, ElementIdentifierType idType, String id, String testData, int timeout) {
        Assert.assertTrue(elementHelper.waitForElementInvisibility(idType,id,timeout));
    }
}
