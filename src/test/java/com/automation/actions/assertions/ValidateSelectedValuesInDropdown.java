package com.automation.actions.assertions;

import com.automation.actions.Action;
import com.automation.helpers.ElementHelper;
import com.automation.model.ElementIdentifierType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.List;

/**
 * Created by Hemant Sushant on 16/10/2016.
 */
public class ValidateSelectedValuesInDropdown implements Action {
    @Override
    @Step("Validate values {3} selected in dropdown {2}")
    public void execute(ElementHelper elementHelper, ElementIdentifierType idType, String id, String testData, int timeout) {
        Select select = new Select(elementHelper.getElement(idType, id, timeout));
        List<WebElement> selectedOptions = select.getAllSelectedOptions();
        String[] values = testData.split(",");
        boolean found=false;
        for (String value:values) {
            for (WebElement option:selectedOptions){
                if(option.getText().equalsIgnoreCase(value.trim())){
                    found = true;
                    break;
                }
            }
            if(!found){
                Assert.fail(value+" not selected in dropdown identified by "+id+"("+idType+")");
            }
            found=false;
        }
    }
}
