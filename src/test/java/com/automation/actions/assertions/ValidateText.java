package com.automation.actions.assertions;

import com.automation.actions.Action;
import com.automation.helpers.ElementHelper;
import com.automation.model.ElementIdentifierType;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by Hemant Sushant on 15/10/2016.
 */
public class ValidateText implements Action {
    @Override
    @Step("Validate {2} matches text {3}")
    public void execute(ElementHelper elementHelper, ElementIdentifierType idType, String id, String testData, int timeout) {
        WebElement element = elementHelper.getElement(idType, id, timeout);
        Assert.assertTrue(element.getText().trim().equalsIgnoreCase(testData.trim()));
    }
}
