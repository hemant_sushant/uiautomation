package com.automation.actions.assertions;

import com.automation.actions.Action;
import com.automation.helpers.ElementHelper;
import com.automation.model.ElementIdentifierType;
import org.testng.Assert;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by Hemant Sushant on 15/10/2016.
 */
public class ValidateElementDisabled implements Action {
    @Override
    @Step("Validate {2} is disabled")
    public void execute(ElementHelper elementHelper, ElementIdentifierType idType, String id, String testData, int timeout) {
        Assert.assertFalse(elementHelper.getElement(idType, id, timeout).isEnabled());
    }
}
