package com.automation.actions.assertions;

import com.automation.actions.Action;
import com.automation.helpers.ElementHelper;
import com.automation.model.ElementIdentifierType;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by Hemant Sushant on 15/10/2016.
 */
public class ValidateCheckBoxStatus implements Action {
    @Override
    @Step("Validate checkbox {2} is {3}")
    public void execute(ElementHelper elementHelper, ElementIdentifierType idType, String id, String testData, int timeout) {
        WebElement element =elementHelper.getElement(idType, id, timeout);
        if (Boolean.parseBoolean(testData.trim())) {
            Assert.assertTrue(element.isSelected());
        }else{
            Assert.assertFalse(element.isSelected());
        }
    }
}
