package com.automation.actions.inputs;

import com.automation.actions.Action;
import com.automation.helpers.ElementHelper;
import com.automation.model.ElementIdentifierType;
import org.openqa.selenium.support.ui.Select;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by Hemant Sushant on 1/09/2016.
 */
public class DeselectAllFromDropdown implements Action {
    @Override
    @Step("Deselecting all options in dropdown {2}")
    public void execute(ElementHelper elementHelper, ElementIdentifierType idType, String id, String testData, int timeout) {
        Select select=new Select(elementHelper.getElement(idType,id, timeout));
        select.deselectAll();
    }
}
