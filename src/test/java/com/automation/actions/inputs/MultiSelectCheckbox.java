package com.automation.actions.inputs;

import com.automation.actions.Action;
import com.automation.helpers.ElementHelper;
import com.automation.model.ElementIdentifierType;
import org.openqa.selenium.WebElement;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by Hemant Sushant on 12/10/2016.
 */
public class MultiSelectCheckbox implements Action {
    @Override
    @Step("Select checkboxes {3}")
    public void execute(ElementHelper elementHelper, ElementIdentifierType idType, String id, String testData, int timeout) {
        //TODO
        WebElement checkBox;
        String[] valuesToBeSelected = testData.split(",");
        for(String value:valuesToBeSelected){
            checkBox = elementHelper.getElement(idType, id, timeout);
            if(!checkBox.isSelected()){
                checkBox.click();
            }
        }
    }
}
