package com.automation.actions.inputs;

import com.automation.actions.Action;
import com.automation.helpers.ElementHelper;
import com.automation.model.ElementIdentifierType;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import ru.yandex.qatools.allure.annotations.Step;


/**
 * Created by Hemant Sushant on 13/10/2016.
 */
public class ExecuteJS implements Action {
    @Override
    @Step("Execute javascript {3}")
    public void execute(ElementHelper elementHelper, ElementIdentifierType idType, String id, String testData, int timeout) {
        WebDriver driver = elementHelper.getDriver();
        if (driver instanceof JavascriptExecutor) {
            if ((id != null && !id.isEmpty()) && (idType != null)) {
                WebElement element = elementHelper.getElement(idType, id, timeout);
                ((JavascriptExecutor)driver).executeScript("arguments[0]."+testData,element);
            } else {
                ((JavascriptExecutor)driver).executeScript(testData);
            }
        } else {
            throw new IllegalStateException("Driver "+driver.getClass()+" does not support JavaScript!");
        }
    }
}
