package com.automation.actions.inputs;

import com.automation.actions.Action;
import com.automation.helpers.ElementHelper;
import com.automation.model.ElementIdentifierType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by Hemant Sushant on 12/10/2016.
 */
public class HoverOverElement implements Action {
    @Override
    @Step("Hover over {2}")
    public void execute(ElementHelper elementHelper, ElementIdentifierType idType, String id, String testData, int timeout) {
        Actions action = new Actions(elementHelper.getDriver());
        WebElement hoverElement = elementHelper.getElement(idType, id, timeout);
        action.moveToElement(hoverElement).perform();
    }
}
