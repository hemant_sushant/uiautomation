package com.automation.actions.inputs;

import com.automation.actions.Action;
import com.automation.constants.RuntimeConstants;
import com.automation.helpers.ElementHelper;
import com.automation.model.ElementIdentifierType;
import org.openqa.selenium.WebElement;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by Hemant Sushant on 13/10/2016.
 */
public class PerformBulkUpload implements Action {
    @Override
    @Step("Perform bulk upload for file {3}")
    public void execute(ElementHelper elementHelper, ElementIdentifierType idType, String id, String testData, int timeout) {
        //TODO with remote webdriver fixes
        WebElement upload=elementHelper.getElement(idType, id, timeout);
        if (!System.getProperty(RuntimeConstants.BROWSER_KEY).equalsIgnoreCase("remote")){
            //upload.sendKeys(getClass().getResource(new String("./uploads/"+testData)));
        }else{
            elementHelper.getDriver();
        }
    }
}
