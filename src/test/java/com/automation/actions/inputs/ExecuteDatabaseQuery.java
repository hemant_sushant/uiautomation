package com.automation.actions.inputs;

import com.automation.actions.Action;
import com.automation.constants.RuntimeConstants;
import com.automation.helpers.ElementHelper;
import com.automation.model.ElementIdentifierType;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by Hemant Sushant on 13/10/2016.
 */
public class ExecuteDatabaseQuery implements Action {

    @Override
    public void execute(ElementHelper elementHelper, ElementIdentifierType idType, String id, String testData, int timeout){
        String connectionString=System.getProperty(RuntimeConstants.DB_CONNECTION_STRING_KEY);

        Connection dbConnection = null;
        Statement statement = null;
        try {
            dbConnection = DriverManager.getConnection(connectionString);
            statement = dbConnection.createStatement();
            //executing the query passed from test data
            statement.execute(testData);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (dbConnection != null) {
                    dbConnection.close();
                }
            } catch (SQLException e) {
                System.out.println("Error closing connection - "+e);
            }
        }
    }

}
