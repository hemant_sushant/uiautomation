package com.automation.actions.inputs;

import com.automation.actions.Action;
import com.automation.helpers.ElementHelper;
import com.automation.model.ElementIdentifierType;
import org.openqa.selenium.Alert;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;

import static com.automation.testRunner.TestHooks.driver;

/**
 * Created by Hemant Sushant on 1/09/2016.
 */
public class ActionAlert implements Action {

    @Override
    @Step("{3} the alert box")
    public void execute(ElementHelper elementHelper, ElementIdentifierType idType, String id, String testData, int timeout) {
        WebDriverWait driverWait = new WebDriverWait(elementHelper.getDriver(),Integer.parseInt(System.getProperty("global.timeout")));
        driverWait.until(ExpectedConditions.alertIsPresent());
        Alert alert = driver.switchTo().alert();
        if(testData.contains("accept")){
            alert.accept();
        }else{
            alert.dismiss();
        }
    }
}
