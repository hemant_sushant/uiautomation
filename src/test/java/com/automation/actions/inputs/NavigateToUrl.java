package com.automation.actions.inputs;

import com.automation.actions.Action;
import com.automation.helpers.ElementHelper;
import com.automation.model.ElementIdentifierType;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by Hemant Sushant on 23/08/2016.
 */
public class NavigateToUrl implements Action {
    @Override
    @Step("Navigate to {3}")
    public void execute(ElementHelper elementHelper, ElementIdentifierType idType, String id, String testData, int timeout) {
        elementHelper.getDriver().get(testData);
    }
}
