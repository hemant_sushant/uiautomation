package com.automation.actions.inputs;

import com.automation.actions.Action;
import com.automation.helpers.ElementHelper;
import com.automation.model.ElementIdentifierType;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by Hemant Sushant on 12/10/2016.
 */
public class ClearText implements Action {
    @Override
    @Step("Clear textbox with {1} identifier {2}")
    public void execute(ElementHelper elementHelper, ElementIdentifierType idType, String id, String testData, int timeout) {
        elementHelper.getElement(idType, id, timeout).clear();
    }
}
