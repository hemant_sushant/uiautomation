package com.automation.loaders;

import com.automation.constants.RuntimeConstants;
import com.automation.helpers.PropertiesHelper;

import static com.automation.constants.RuntimeConstants.BROWSER_KEY;
import static com.automation.constants.RuntimeConstants.DB_CONNECTION_STRING_KEY;

/**
 * Class used to load all properties from config before the start of execution
 */
public class ConfigLoader {

    public void loadConfigProperties(){
        PropertiesHelper propertiesReader =new PropertiesHelper(System.getProperty(RuntimeConstants.CONFIG_FILE_KEY));
        //setting the default value of timeout as system property from properties file if not overridden in commandline
        if (System.getProperty(RuntimeConstants.TIMEOUT_KEY)==null || System.getProperty(RuntimeConstants.TIMEOUT_KEY).trim().isEmpty()) {
            try {
                int timeout = Integer.parseInt(propertiesReader.getProperty(RuntimeConstants.TIMEOUT_KEY));
                System.setProperty(RuntimeConstants.TIMEOUT_KEY, String.valueOf(timeout));
            } catch (NumberFormatException e) {
                throw new RuntimeException("Invalid globalTimeout set. Timeout must be a number",e);
            }
        }

        //setting the location of object repository for the test run
        System.setProperty(RuntimeConstants.OBJECT_REPO_FOLDER_KEY,propertiesReader.getProperty(RuntimeConstants.OBJECT_REPO_FOLDER_KEY).trim());
        System.out.println(propertiesReader.getProperty(RuntimeConstants.OBJECT_REPO_FOLDER_KEY).trim());

        //setting the browser width and height
        if (System.getProperty(RuntimeConstants.BROWSER_HEIGHT_KEY)==null || System.getProperty(RuntimeConstants.BROWSER_HEIGHT_KEY).trim().isEmpty()){
            System.setProperty(RuntimeConstants.BROWSER_HEIGHT_KEY,propertiesReader.getProperty(RuntimeConstants.BROWSER_HEIGHT_KEY).trim());
        }
        if (System.getProperty(RuntimeConstants.BROWSER_WIDTH_KEY)==null || System.getProperty(RuntimeConstants.BROWSER_WIDTH_KEY).trim().isEmpty()){
            System.setProperty(RuntimeConstants.BROWSER_WIDTH_KEY,propertiesReader.getProperty(RuntimeConstants.BROWSER_WIDTH_KEY).trim());
        }

        //setting the browser type
        if (System.getProperty(RuntimeConstants.BROWSER_KEY)==null || System.getProperty(RuntimeConstants.BROWSER_KEY).trim().isEmpty()) {
            System.setProperty(BROWSER_KEY,propertiesReader.getProperty(BROWSER_KEY).trim());
        }

        //setting the db connection string
        if (System.getProperty(RuntimeConstants.DB_CONNECTION_STRING_KEY)==null || System.getProperty(RuntimeConstants.DB_CONNECTION_STRING_KEY).trim().isEmpty()) {
            System.setProperty(DB_CONNECTION_STRING_KEY,propertiesReader.getProperty(DB_CONNECTION_STRING_KEY).trim());
        }
    }

}
