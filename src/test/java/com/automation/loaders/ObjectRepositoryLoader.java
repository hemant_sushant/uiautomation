package com.automation.loaders;

import com.automation.constants.RuntimeConstants;
import com.automation.model.ElementIdentifierType;
import com.automation.model.ElementReference;
import com.automation.model.ObjectRepository;
import com.automation.model.Page;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.RegexFileFilter;

import java.io.*;
import java.util.*;

/**
 * Created by Hemant Sushant on 30/10/2016.
 */
public class ObjectRepositoryLoader {
    public void loadObjectRepository() {
        String fileSeparator = System.getProperty("file.separator");
        Collection<File> files = readAllPropertyFilesRecursive("src"+fileSeparator+"test"+fileSeparator+"resources"+fileSeparator+System.getProperty(RuntimeConstants.OBJECT_REPO_FOLDER_KEY));
        for (File file:files){
            Page page = new Page(file.getName(),file.getPath(),loadElementReferences(file));
            //setting the page with identifier as the file name - will be converted to upper case after removing '.properties'
            ObjectRepository.getInstance().addPage(file.getName().trim(),page);
        }
    }

    private Collection<File> readAllPropertyFilesRecursive(String directoryPath){
        Collection<File> files = FileUtils.listFiles(new File(System.getProperty("user.dir")+System.getProperty("file.separator")+directoryPath),
                new RegexFileFilter("^(.*\\.properties)$"), DirectoryFileFilter.DIRECTORY);
        return files;
    }

    private Map<String, ElementReference> loadElementReferences(File file){
        InputStream input;
        Properties prop = new Properties();
        Map<String, ElementReference> elementReferences = new HashMap<>();
        try {
            input = new FileInputStream(file);
            prop.load(input);
        }catch (FileNotFoundException fnfe) {
            //TODO : Not sure if this is ok, how to ensure this is noted by the user
            System.out.println("File with element references "+file.getName()+" not found in path "+file.getAbsolutePath()+" :"+fnfe);
        } catch (IOException ioe) {
            System.out.println("Error reading element references from file "+file.getName()+" :"+ioe);
        }
        for (Object key: prop.keySet()){
            String referenceString = prop.getProperty((String)key);
            String[] referenceSet = referenceString.split(":");
            if (referenceSet.length!=2){
                //TODO: change to custom exception
                throw new RuntimeException("Reference "+referenceString+" with element id "+key+" defined in file "+file.getName()+" is invalid. Please re-verify");
            }
            ElementIdentifierType idType = ElementIdentifierType.fromString(referenceSet[0].trim());
            if (idType == null){
                //TODO: change to custom exception
                throw new RuntimeException("Id type "+referenceSet[0].trim()+" defined in file "+file.getName()+" does not match with available element identification methods");
            }
            String identifier = referenceSet[1].trim();
            ElementReference elementReference = new ElementReference((String)key,idType,identifier);
            elementReferences.put((String)key, elementReference);
        }
        return elementReferences;
    }
}
