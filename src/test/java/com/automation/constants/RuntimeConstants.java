package com.automation.constants;

/**
 * Created by Hemant Sushant on 16/10/2016.
 */
public class RuntimeConstants {
    public static final String CONFIG_FILE_KEY = "framework.config.file";
    public static final String TIMEOUT_KEY = "global.timeout";
    public static final String BROWSER_KEY = "browser";
    public static final String BROWSER_HEIGHT_KEY = "browser.height";
    public static final String BROWSER_WIDTH_KEY = "browser.width";
    public static final String DB_CONNECTION_STRING_KEY="db.connection.string";
    public static final String OBJECT_REPO_FOLDER_KEY="object.repo.folder";
}
