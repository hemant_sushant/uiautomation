package com.automation.testRunner;

import com.automation.loaders.ConfigLoader;
import com.automation.loaders.ObjectRepositoryLoader;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.testng.ITestContext;
import org.testng.annotations.BeforeSuite;

@CucumberOptions(features={"src/test/resources/features"}, glue = "com.automation", plugin={"com.github.kirlionik.cucumberallure.AllureReporter"})
public class CukesRunner extends AbstractTestNGCucumberTests {
    @BeforeSuite
    public void beforeSuite(ITestContext context){
        ObjectRepositoryLoader objectRepositoryLoader = new ObjectRepositoryLoader();
        ConfigLoader configLoader = new ConfigLoader();
        configLoader.loadConfigProperties();
        objectRepositoryLoader.loadObjectRepository();
        /*context.getSuite().getXmlSuite().setParallel(XmlSuite.ParallelMode.CLASSES);
        context.getSuite().getXmlSuite().setThreadCount(3);*/
    }

}
