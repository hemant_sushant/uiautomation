package com.automation.testRunner;


import com.automation.constants.RuntimeConstants;
import com.automation.helpers.WebDriverFactory;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.*;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.remote.RemoteWebDriver;
import ru.yandex.qatools.allure.annotations.Attachment;

import java.util.logging.Level;

/**
 * Created by Hemant Sushant on 14/08/2016.
 */
public class TestHooks {
    private final String BROWSER_KEY = RuntimeConstants.BROWSER_KEY;
    public static WebDriver driver;
    private String browser=System.getProperty(BROWSER_KEY);


    /**
     * Before method invoked before each scenario run in cucumber
     */
    @Before
    public void beforeMethod(){
        driver = WebDriverFactory.createWebDriver(browser);
        Capabilities caps=((RemoteWebDriver)driver).getCapabilities();
        System.setProperty("browser.version",caps.getVersion());
        System.setProperty("remote.os.name",caps.getPlatform().toString());
    }

    /**
     * After method invoked after each scenario run in cucumber
     */
    @After
    public void afterMethod(Scenario scenario){
        //TODO: find workaround for issue with marionette and browser logging
        getBrowserLogs(driver);
        if(scenario.isFailed()) {
            saveHtmlAttachment(driver);
            try {
                scenario.write("Current Page URL is " + driver.getCurrentUrl());
                byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
                scenario.embed(screenshot, "image/png");
            } catch (WebDriverException we) {
                System.err.println(we.getMessage());
            }
        }
        System.out.println("Closing driver...");
        driver.close();
    }

    @Attachment(value = "Failure Page")
    private String saveHtmlAttachment(WebDriver driver){
        return driver.getPageSource();
    }

    @Attachment(value = "Browser logs")
    private String getBrowserLogs(WebDriver driver){
        LogEntries logEntries = driver.manage().logs().get(LogType.BROWSER);
        String logs="Browser Logs:"+System.getProperty("line.separator");
        for (LogEntry entry : logEntries) {
            if (entry.getLevel()!=null && entry.getLevel()!= Level.WARNING) {
                logs= logs+entry.toString()+ System.getProperty("line.separator");
            }
        }

        logs = logs +System.getProperty("line.separator")+"--------------------------------------------------"
                +System.getProperty("line.separator")+"Driver Logs:"+System.getProperty("line.separator");
        logEntries = driver.manage().logs().get(LogType.DRIVER);
        for (LogEntry entry : logEntries) {
            if (entry.getLevel()!=null && entry.getLevel()!= Level.WARNING) {
                logs= logs+entry.toString()+ System.getProperty("line.separator");
            }
        }
        return logs;
    }

}
