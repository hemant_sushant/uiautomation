package com.automation.model;

/**
 * Created by Hemant Sushant on 29/10/2016.
 */
public class ElementReference {
    private String elementId;
    private ElementIdentifierType elementIdentificationType;
    private String elementIdentifier;

    public ElementReference(String elementId, ElementIdentifierType elementIdentificationType, String elementIdentifier) {
        this.elementId = elementId;
        this.elementIdentificationType = elementIdentificationType;
        this.elementIdentifier = elementIdentifier;
    }

    public ElementIdentifierType getElementIdentificationType() {
        return elementIdentificationType;
    }

    public String getElementIdentifier() {
        return elementIdentifier;
    }
}
