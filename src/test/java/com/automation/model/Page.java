package com.automation.model;

import java.util.Map;

/**
 * Created by Hemant Sushant on 30/10/2016.
 */
public class Page {
    private String pageId;
    private Map<String, ElementReference> elementReferences;
    private String fileLocation;

    public Page(String pageId, String fileLocation, Map<String, ElementReference> elementReferences) {
        this.pageId = pageId;
        this.elementReferences = elementReferences;
        this.fileLocation = fileLocation;
    }

    public ElementReference getElementReference(String elementId) {
        return elementReferences.get(elementId);
    }

    public String getFileLocation() {
        return fileLocation;
    }
}
