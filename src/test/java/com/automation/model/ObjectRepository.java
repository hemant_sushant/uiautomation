package com.automation.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Hemant Sushant on 1/11/2016.
 */
public class ObjectRepository {
    private static final ObjectRepository REPOSITORY = new ObjectRepository();
    private Map<String,Page> pages;

    private ObjectRepository(){
        pages = new HashMap<>();
    }

    public static ObjectRepository getInstance(){
        return REPOSITORY;
    }

    public void addPage(String fileName, Page page) {
        //setting the page with identifier as the file name in upper case after removing '.properties'
        String pageId = fileName.substring(0,fileName.length()-11).toUpperCase();
        //making sure that all file names are unique in object repo before adding
        if (!pages.containsKey(pageId)) {
            pages.put(pageId, page);
        } else {
            throw new RuntimeException("Duplicate files found in object repository. Please rename file "+page.getFileLocation()+" as it is a duplicate to "+getPage(pageId).getFileLocation());
        }
    }

    public Page getPage(String pageId) {
        return pages.get(pageId);
    }
}
