package com.automation.model;

/**
 * Created by Hemant Sushant on 30/10/2016.
 */
public enum ElementIdentifierType {
    //TODO: complete and make cHAnges to whereever identifiers are used
    ID("id"),
    NAME("name"),
    CLASSNAME("classname"),
    LINKTEXT("linktext"),
    PARTIALLINKTEXT("partiallinktext"),
    XPATH("xpath"),
    CSSSELECTOR("cssselector"),
    TAGNAME("tagname");

    private String value;
    ElementIdentifierType(String value) {
        this.value=value;
    }

    public String value(){
        return value;
    }

    public static ElementIdentifierType fromString(String value) {
        if (value != null && !value.trim().isEmpty()) {
            for (ElementIdentifierType e : ElementIdentifierType.values()) {
                if (value.trim().equalsIgnoreCase(e.value)) {
                    return e;
                }
            }
        }
        return null;
    }
}
