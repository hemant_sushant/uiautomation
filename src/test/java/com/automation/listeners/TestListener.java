package com.automation.listeners;

import com.automation.helpers.PropertiesHelper;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Hemant Sushant on 26/08/2016.
 */
public class TestListener extends TestListenerAdapter {

    @Override
    public void onTestStart(ITestResult result) {

    }

    @Override
    public void onTestSuccess(ITestResult result) {
        System.out.println("on test method " + getTestMethodName(result) + " success");
    }

    @Override
    public void onTestFailure(ITestResult result) {
        System.out.println("on test method " + getTestMethodName(result) + " failure");
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        System.out.println("test method " + getTestMethodName(result) + " skipped");
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
        System.out.println("test failed but within success % " + getTestMethodName(result));
    }

    @Override
    public void onStart(ITestContext context) {
        System.out.println("on start of test " + context.getName());
    }

    @Override
    public void onFinish(ITestContext context) {
        Map<String,String> environment =new HashMap<>();
        //TODO Add more parameters to the environment
        environment.put("browser",System.getProperty("browser"));
        environment.put("browser.version",System.getProperty("browser.version"));
        environment.put("os.name",System.getProperty("remote.os.name"));
        environment.put("java.version",System.getProperty("java.version"));
        PropertiesHelper propertiesHelper=new PropertiesHelper();
        String fileSeparator = System.getProperty("file.separator");
        propertiesHelper.generatePropertyFile("target"+fileSeparator+"allure-results"+fileSeparator+"environment.properties",environment);
    }

    private static String getTestMethodName(ITestResult result) {
        return result.getMethod().getConstructorOrMethod().getName();
    }

}
