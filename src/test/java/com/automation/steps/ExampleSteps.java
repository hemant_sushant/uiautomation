package com.automation.steps;

import com.automation.actions.Action;
import com.automation.actions.assertions.ValidateElementPresent;
import com.automation.actions.inputs.ClickElement;
import com.automation.actions.inputs.NavigateToUrl;
import com.automation.actions.inputs.SendKeys;
import com.automation.helpers.ElementHelper;
import com.automation.model.ElementIdentifierType;
import com.automation.model.ObjectRepository;
import com.automation.testRunner.TestHooks;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;

public class ExampleSteps {
    WebDriver driver = TestHooks.driver;
    ElementHelper elementHelper;
    public ExampleSteps() {
        elementHelper = new ElementHelper(driver);
    }

    @Given("^I navigate to bing")
    public void iNavigateToGoogle() throws Throwable {
        Action action=new NavigateToUrl();
        action.execute(elementHelper,null,"","http://www.bing.com",0);
    }
    @When("^I search for \"([^\"]*)\"$")
    public void iSearchFor(String searchText) throws Throwable {
        Action sendKeys=new SendKeys();
        sendKeys.execute(elementHelper,ElementIdentifierType.fromString("name"),"q",searchText,0);
        Action clickElement=new ClickElement();
        clickElement.execute(elementHelper, ElementIdentifierType.fromString("name"),"go","",0);
    }
    @Then("^I should be able to view search results$")
    public void iShouldBeAbleToViewSearchResults() throws Throwable {
        Action validateElementPresent=new ValidateElementPresent();
        validateElementPresent.execute(elementHelper,ObjectRepository.getInstance().getPage("SEARCHPAGE").getElementReference("searchpage.searchresult").getElementIdentificationType(), ObjectRepository.getInstance().getPage("SEARCHPAGE").getElementReference("searchpage.searchresult").getElementIdentifier(),"",0);
    }

    @When("^I search for \"([^\"]*)\" and \"([^\"]*)\"$")
    public void iSearchForAnd(String arg0, String arg1) throws Throwable {
        System.out.println("##################"+arg0+"######"+arg1+"###########");
    }
}